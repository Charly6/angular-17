import { Component, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {

  @Output() EventoMensaje1 = new EventEmitter<string>();
  @Output() EventoMensaje2 = new EventEmitter<string>();
  @Output() EventoMensaje3 = new EventEmitter<string>();
  @Output() EventoMensaje4 = new EventEmitter<string>();
  @Output() EventoMensaje5 = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    this.EventoMensaje1.emit("Mensaje 1 desde el hijo => padre")
    this.EventoMensaje2.emit("Mensaje 2 desde el hijo => padre")
    this.EventoMensaje3.emit("Mensaje 3 desde el hijo => padre")
    this.EventoMensaje4.emit("Mensaje 4 desde el hijo => padre")
    this.EventoMensaje5.emit("Mensaje 5 desde el hijo => padre")
  }

}
