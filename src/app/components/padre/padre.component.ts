import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  textoPadre1 = ""
  textoPadre2 = ""
  textoPadre3 = ""
  textoPadre4 = ""
  textoPadre5 = ""

  constructor() { }

  ngOnInit(): void {
  }

  recibirMensaje1($event:string) {
    console.log($event);
    this.textoPadre1 = $event
  }
  
  recibirMensaje2($event:string) {
    console.log($event);
    this.textoPadre2 = $event
  }
  
  recibirMensaje3($event:string) {
    console.log($event);
    this.textoPadre3 = $event
  }
  
  recibirMensaje4($event:string) {
    console.log($event);
    this.textoPadre4 = $event
  }
  
  recibirMensaje5($event:string) {
    console.log($event);
    this.textoPadre5 = $event
  }
}
